package location;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

public class patternDongCheck {

    public static void main(String[] args) throws Exception {
        // TODO Auto-generated method stub

        Pattern DONG_CHECK =  ADDRESS_PATTERN.CUSTOM.DONG_CHECK.getPettern();
        Pattern NUMBER_OUT =  ADDRESS_PATTERN.CUSTOM.NUMBER_OUT.getPettern();
        //광주 광산구 산정동 1059 중흥 s클래스103동502호
        //서울 은평구 서울시 은평구 진관동 16-29 은평지웰테라스 1509동 403호
        //인천 부평구 삼산동 삼1동 상2동 상3동 453-1 삼산타운7단지715동 1504호

        //가|나|다|라|마|바|사|아|자|차|카|타|파|하
        //(\\d{1,4}|[a-zA-Z]{1}\\d{1,3})+
        //|([a-zA-Z]{1}+\\d{1,3})

        String addr = "서울 도봉구 창1동  대우아파트103-1907호";
        String result = "서울 강남구 논현1동 마일스디오빌 58-2 1209호";
        //addr = "서울 도봉구 창2동 대우이너너너너너너너너너너스빌리지726번지";

        //서울 강남구 논현1동 마일스디오빌 논현동 58-2 1209호
        String str = getMatch(DONG_CHECK, addr);
        String diff = getMatch(DONG_CHECK, result);

        System.out.println("str : "+str);
        System.out.println("diff : "+diff);

        if(!StringUtils.isEmpty(str) && !StringUtils.isEmpty(diff)){

            str = getMatchOut(NUMBER_OUT, str);
            diff = getMatchOut(NUMBER_OUT, diff);
            System.out.println("str0 : "+str);
            System.out.println("length : "+str.trim().length());


            if(str.trim().length() == 2 ){

                if(diff.trim().length() > 1 ){
                    str = str.trim().substring(0,1);
                    System.out.println("str1 : "+str);
                    diff = diff.trim().substring(0,1);

                    if(!str.equals(diff))
                        System.out.println("str1 : "+str+", diff1 : "+ diff+" | 정확도 낮음");
                }else{
                    System.out.println("else str1 : "+str+", diff1 : "+ diff+" | 정확도 낮음");
                }


            }else if( str.trim().length() > 2 ){

                if(diff.trim().length() > 2 ){
                    str = str.trim().substring(0, 2);
                    System.out.println("str2 : "+str);
                    diff = diff.trim().substring(0, 2);

                    if(!str.equals(diff))
                        System.out.println("str2 : "+str+", diff2 : "+ diff+" | 정확도 낮음");

                }else{
                    System.out.println("else str2 : "+str+", diff2 : "+ diff+" | 정확도 낮음");
                }

            }
        }
        //str = dupleDongMatch(DUPLE_DONG, str);
        //System.out.println(str);

    }

    /**
     * 정규식 패턴 매칭(매칭된 패턴과 일치한 것만 가져옴)
     * @Method Name : getMatch
     * @param p
     * @param target
     * @return
     */
    private static String getMatch(Pattern p, String target) throws Exception {

        Matcher m = p.matcher(target);

        if (m.find()) target=m.group();
        else target = null;

        return target;
    }

    /**
     * 정규식 패턴 매칭(매칭된 패턴 제거)
     * @Method Name : getMatchOut
     * @param p
     * @param target
     * @return
     */
    private static String getMatchOut(Pattern p, String target) throws Exception {

        Matcher m = p.matcher(target);

        target =  m.replaceAll("");

        return target;
    }


}
